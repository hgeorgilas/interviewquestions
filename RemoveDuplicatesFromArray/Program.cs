﻿using System;

namespace RemoveDuplicatesFromArray
{
    //Remove duplicates from array without using a collection class.
    //replacing duplicate with 0 is allowed.
    class Program
    {
        static void Main(string[] args)
        {
            int[] array = new int[] { 1, 2, 3, 4, 5, 6, 6, 7, 7, 8, 8, 8, 8, 1, 1, 2 };
            int[] withoutDuplicates = ReplaceDuplicates(array);
            Console.WriteLine(string.Join(',', withoutDuplicates));
            Console.ReadLine();
        }

        // Time complexity O(n), Space complexity O(n)
        static int[] ReplaceDuplicates(int[] array)
        {
            //sort array to group duplicates
            Array.Sort(array);

            var element = 0;
            int[] newArray = new int[array.Length];
            //assign first element in case we have a duplicate first element in sorted array.
            newArray[0] = array[0];

            for (int i = 1; i < array.Length; i++)
            {
                if (array[i] == array[element])
                {
                    newArray[i] = 0;
                }
                else
                {
                    newArray[i] = array[i];
                }

                element++;
            }

            return newArray;
        }
    }
}
