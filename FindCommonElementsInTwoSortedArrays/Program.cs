﻿using System;
using System.Collections.Generic;

namespace FindCommonElementsInTwoSortedArrays
{
    class FindCommonElementsInTwoSortedArrays
    {

        static void Main(string[] args)
        {
            int[] array1 = new int[] { 1, 2, 3, 4, 5, 6, 8 };
            int[] array2 = new int[] { 1, 2, 4, 5, 8 };
            FindCommonElements(array1, array2);
            Console.ReadLine();
        }

        static void FindCommonElements(int[] array1, int[] array2)
        {

            int pointer1 = 0;
            int pointer2 = 0;
            List<int> commonElements = new List<int>();
            while (array1.Length > pointer1 && array2.Length > pointer2)
            {
                if (array1[pointer1] == array2[pointer2])
                {
                    commonElements.Add(array1[pointer1]);
                    pointer1++;
                    pointer2++;
                }
                else if (array1[pointer1] > array2[pointer2])
                {
                    pointer2++;
                }
                else
                {
                    pointer1++;
                }
            }
            Console.WriteLine("array 1: " + string.Join(',', array1));
            Console.WriteLine("array 2: " + string.Join(',', array2));
            Console.WriteLine("Common: " + string.Join(',', commonElements));
        }
    }
}
