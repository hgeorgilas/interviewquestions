﻿using System;
using System.Collections.Generic;

namespace FindDuplicateInArray
{
    class Program
    {

        static void Main(string[] args)
        {
            int[] array = new int[] { 1, 2, 3, 4, 4, 5, 6, 7, 8 };
            var result = FindDuplicate(array);
            Console.WriteLine(result);
            Console.ReadLine();
        }

        static int FindDuplicate(int[] array)
        {

            int result = 0;

            Dictionary<int, int> dic = new Dictionary<int, int>();

            for (int i = 0; i < array.Length; i++)
            {
                if (dic.ContainsKey(array[i]))
                {
                    dic[array[i]]++;
                }
                else
                {
                    dic[array[i]] = 1;
                }
            }

            foreach (KeyValuePair<int, int> entry in dic)
            {
                if (entry.Value > 1)
                {
                    result = entry.Key;
                    break;
                }
            }

            return result;

        }
    }
}
