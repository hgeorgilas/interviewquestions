﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BFS
{
    //implement BFS algorithm
    class Program
    {

        static void Main(string[] args)
        {
            //create a sample tree
            Node Aleft = new Node("A-left", null, null);
            Node Aright = new Node("A-right", null, null);
            Node A = new Node("A", Aleft, Aright);

            Node Bleft = new Node("B-left", null, null);
            Node Bright = new Node("B-right", null, null);
            Node B = new Node("B", Bleft, Bright);

            Node root = new Node("Root", A, B);

            //BFS Traversal
            Console.WriteLine("[BFS]");
            new BFS().Traverse(root);
            Console.WriteLine();
            Console.WriteLine("---------");

            //DFS Traversal
            Console.WriteLine("[DFS]");
            Console.WriteLine("Pre order");
            new DFS().TraversePreorder(root);
            Console.WriteLine();
            new DFS().TraversePreOrderIteratively(root);
            Console.WriteLine("\n---------");

            Console.WriteLine("In order");
            new DFS().TraverseInOrder(root);
            Console.WriteLine();
            new DFS().TraverseInOrderIteratively(root);
            Console.WriteLine("\n---------");

            Console.WriteLine("Post order");
            new DFS().TraversePostOrder(root);
            Console.WriteLine();
            new DFS().TraversePostOrderIteratively(root);

            Console.ReadLine();

        }

    }

    #region BFS
    // Time complexity O(n)
    // Space complexity O(n)
    class BFS
    {
        private Queue<Node> queue = new Queue<Node>();

        public void Traverse(Node node)
        {
            if (node == null) return;
            queue.Enqueue(node);
            while (q.Count != 0)
            {
                Node current = queue.Dequeue();
                PrintData(current.data);
                if (current.left != null) queue.Enqueue(current.left);
                if (current.right != null) queue.Enqueue(current.right);
            }
        }

        void PrintData(string data)
        {
            Console.Write(data + " ");
        }
    }
    #endregion

    #region DFS
    // Time complexity O(n)
    // Space complexity O(n)
    class DFS
    {

        // preorder = root > left > right
        public void TraversePreorder(Node node)
        {
            if (node == null) return;
            PrintData(node.data);
            TraversePreorder(node.left);
            TraversePreorder(node.right);
        }

        public void TraversePreOrderIteratively(Node node)
        {
            if (node == null) return;

            Stack<Node> stack = new Stack<Node>();
            stack.Push(node);

            while (stack.Count != 0)
            {
                var popped = stack.Pop();
                PrintData(popped.data);
                if (popped.right != null)
                {
                    stack.Push(popped.right);
                }
                if (popped.left != null)
                {
                    stack.Push(popped.left);
                }
            }

        }

        // inorder = left > root > right
        public void TraverseInOrder(Node node)
        {
            if (node == null) return;
            TraverseInOrder(node.left);
            PrintData(node.data);
            TraverseInOrder(node.right);
        }

        public void TraverseInOrderIteratively(Node node)
        {
            Stack<Node> stack = new Stack<Node>();
            Node current = node;
            while (true)
            {
                if (current != null)
                {
                    stack.Push(current);
                    current = current.left;
                }
                else
                {
                    if (stack.Count == 0)
                    {
                        break;
                    }
                    current = stack.Pop();
                    PrintData(current.data);
                    current = current.right;
                }
            }
        }

        // postorder = left  > right > root
        public void TraversePostOrder(Node node)
        {
            if (node == null) return;
            TraversePostOrder(node.left);
            TraversePostOrder(node.right);
            PrintData(node.data);

        }

        public void TraversePostOrderIteratively(Node node)
        {
            Node current = node;
            Stack<Node> stack = new Stack<Node>();
            while (current != null || stack.Count != 0)
            {
                if (current != null)
                {
                    stack.Push(current);
                    current = current.left;
                }
                else
                {
                    Node temp = stack.Peek().right;
                    if (temp == null)
                    {
                        temp = stack.Pop();
                        PrintData(temp.data);
                        while (stack.Count != 0 && temp == stack.Peek().right)
                        {
                            temp = stack.Pop();
                            PrintData(temp.data);
                        }
                    }
                    else
                    {
                        current = temp;
                    }
                }
            }
        }

        void PrintData(string data)
        {
            Console.Write(data + " ");
        }

    }
    #endregion

    class Node
    {
        public string data { get; set; }
        public Node left { get; set; }
        public Node right { get; set; }

        public Node(string data, Node left, Node right)
        {
            this.data = data;
            this.left = left;
            this.right = right;
        }

    }


}
