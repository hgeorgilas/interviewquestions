﻿using System;
using System.Collections.Generic;

namespace FindPairsThatSumToInputNumber
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] array = new int[] { 1, 2, 3, 4, 5, 6, 7, 8 };
            int number = 7;
            //for unsorted array
            FindPairsThatSum(array, number);
            Console.ReadLine();
        }

        static void FindPairsThatSum(int[] array, int number)
        {
            var set = new HashSet<int>();
            for (int i = 0; i < array.Length; i++)
            {
                int target = number - array[i];
                if (Array.IndexOf(array, target) > -1 && (!set.Contains(array[i]) || !set.Contains(target)))
                {
                    Console.WriteLine(array[i] + " + " + target);
                    //add to set to ignore duplicate values
                    //eg 6 + 1 = 7
                    //eg 1 + 6 = 7
                    set.Add(array[i]);
                    set.Add(target);
                }
            }
        }
    }
}
