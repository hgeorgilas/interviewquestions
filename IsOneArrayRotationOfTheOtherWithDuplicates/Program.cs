﻿using System;

namespace IsOneArrayRotationOfTheOtherWithDuplicates
{
    class IsOneArrayRotationOfTheOtherWithDuplicates
    {
        static void Main(string[] args)
        {
            var arrayA = new int[] { 1, 2, 3, 4, 5, 6 };
            var arrayB = new int[] { 4, 5, 6, 1, 2, 3 };
            //to calculate that for an array with duplicates
            //we can duplicate arrayA to a new array and check if 
            bool result = IsRotation(arrayA, arrayB);
            Console.WriteLine(result);
            Console.ReadLine();
        }

        static bool IsRotation(arrayA, arrayB) {

            return true;
        }
    }
}
