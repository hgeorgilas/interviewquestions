﻿using System;

namespace CheckIfABTreeIsABST
{
    /**
    * A binary search tree (BST) is a node based binary tree data structure which has the following properties.
    • The left subtree of a node contains only nodes with keys less than the node’s key.
    • The right subtree of a node contains only nodes with keys greater than the node’s key.
    • Both the left and right subtrees must also be binary search trees.
    */
    class Program
    {
        static void Main(string[] args)
        {
            Node Aleft = new Node(1, null, null);
            Node Aright = new Node(6, null, null);
            Node A = new Node(4, Aleft, Aright);

            Node Bleft = new Node(7, null, null);
            Node Bright = new Node(12, null, null);
            Node B = new Node(9, Bleft, Bright);

            Node root = new Node(7, A, B);

            var result = IsBinarySearchTree(root, Int32.MinValue, Int32.MaxValue);
            Console.WriteLine(result);
            Console.ReadLine();
        }


        static bool IsBinarySearchTree(Node node, int minValue, int maxValue)
        {
            if (node == null) return true;

            var result = false;

            if (node.data > minValue && node.data < maxValue && IsBinarySearchTree(node.left, minValue, node.data) && IsBinarySearchTree(node.right, node.data, maxValue))
            {
                result = true;
            }
            return result;
        }



    }

    class Node
    {
        public int data { get; set; }
        public Node left { get; set; }
        public Node right { get; set; }

        public Node(int data, Node left, Node right)
        {
            this.data = data;
            this.left = left;
            this.right = right;
        }

        public bool IsLeaf()
        {
            return this.left == null && this.right == null;
        }

    }
}
