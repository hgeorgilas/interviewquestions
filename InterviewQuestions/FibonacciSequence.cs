﻿using System;

namespace InterviewQuestions
{
    class FibonacciSequence
    {
        static void Main(string[] args)
        {
            int fibNumber = Fibonacci(20);
            Console.WriteLine(fibNumber);

            int fibNumberInPlace = FibonacciInPlace(20);
            Console.WriteLine(fibNumberInPlace);

            Console.ReadKey();
        }

        /**
         * Iterative solution
         * Time complexity O(n)
         * Space complexity O(n)
         **/
        static int Fibonacci(int number)
        {

            if (number == 0) return 0;
            if (number == 1) return 1;

            //create a new array of number + 1
            int[] fibNumbers = new int[number + 1];

            //add first two numbers 0 and 1 to fib sequence
            fibNumbers[0] = 0;
            fibNumbers[1] = 1;

            // start calculating fib sequence from 2 because 
            // we already added 0 and 1 to our array
            for (int i = 2; i <= number; i++)
            {
                fibNumbers[i] = fibNumbers[i - 1] + fibNumbers[i - 2];
            }
            return fibNumbers[fibNumbers.Length - 1];
        }

        /**
         * Iterative solution
         * Time complexity O(n)
         * Space complexity O(1)
         **/
        static int FibonacciInPlace(int number)
        {
            if (number == 0) return 0;
            if (number == 1) return 1;

            int firstnumber = 0;
            int secondnumber = 1;
            int result = 0;

            //just use a temp variable to store the next two numbers to add
            for (int i = 2; i <= number; i++)
            {
                result = firstnumber + secondnumber;
                firstnumber = secondnumber;
                secondnumber = result;
            }
            return result;
        }
    }
}
