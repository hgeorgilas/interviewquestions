﻿using System;

namespace PowerOfNumber
{
    class PowerOfNumber
    {
        static void Main(string[] args)
        {
            bool result = IsPowerOfNumber(number: 4, powerOf: 2);
            Console.WriteLine(result);
            Console.ReadLine();
        }

        static bool IsPowerOfNumber(int number, int powerOf)
        {

            bool result = false;

            // while number mod powerOf returns 0 
            // keep dividing with powerOf
            while (number % powerOf == 0)
            {
                number = number / powerOf;
            }

            // if the remaining number equals 1 it's power of powerOf
            if (number == 1) result = true;

            return result;
        }
    }
}
