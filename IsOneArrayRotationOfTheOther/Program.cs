﻿using System;

namespace IsOneArrayRotationOfTheOther
{
    class IsOneArrayRotationOfTheOther
    {
        static void Main(string[] args)
        {
            var arrayA = new int[] { 1, 2, 3, 4, 5, 6 };
            var arrayB = new int[] { 4, 5, 6, 1, 2, 3 };

            bool result = IsRotation(arrayA, arrayB);
            Console.WriteLine(result);
            Console.ReadLine();
        }

        static bool IsRotation(int[] arrayA, int[] arrayB)
        {
            // if not same length return false.
            if (arrayA.Length != arrayB.Length) { return false; }

            int pointer = arrayA[0];
            int index = -1;

            // find if there is a same element in 
            // array_b and return index.
            for (int i = 0; i < arrayB.Length; i++)
            {
                if (arrayB[i] == pointer)
                {
                    index = i;
                    break;
                }
            }

            // we did not find a same element so a rotation is not possible.
            if (index == -1) { return false; }


            for (int i = 0; i < arrayA.Length; i++)
            {
                //bIndex must be equal with index we found the same element + i mod length of arrayA.
                int bIndex = (index + i) % arrayA.Length;
                if (arrayA[i] != arrayB[bIndex])
                {
                    return false;

                }
            }


            return true;

        }
    }
}
