﻿using System;
using System.Collections.Generic;

namespace FindIfLIstOfStringContainsAnagrams
{
    // find if a list of strings contains at least two words
    // which are anagrams
    class FindIfLIstOfStringContainsAnagrams
    {
        static void Main(string[] args)
        {
            List<string> list = new List<string> { "apple", "banana", "orange","nabana" };
            var result = DoesItContainAnagrams(list);
            Console.WriteLine(result);
            Console.ReadLine();
        }

        static bool DoesItContainAnagrams(List<string> list)
        {
            HashSet<string> set = new HashSet<string>();
            bool result = false;
            foreach (string word in list)
            {
                var arrayWord = word.ToCharArray();
                Array.Sort(arrayWord);
                if (set.Contains(new String(arrayWord)))
                {
                    result = true;
                    break;
                }
                else
                {
                    set.Add(new String(arrayWord));
                }
            }

            return result;

        }
    }
}
