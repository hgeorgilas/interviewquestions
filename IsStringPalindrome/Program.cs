﻿using System;

namespace IsStringPalindrome
{
    //if string is equal with it's reversed edition is called palindrome
    class Program
    {
        static void Main(string[] args)
        {
            string palindrome = "level";
            string nonPalindrome = "hello";

            Console.WriteLine(IsPalindrome(palindrome));
            Console.WriteLine(IsPalindrome(nonPalindrome));

            Console.ReadLine();
        }

        static bool IsPalindrome(string word)
        {

            var wordAsArray = word.ToCharArray();
            var reversed = string.Empty;
            // we could use Array.Reverse here but for the sake of not using a helper method
            // we are going to use a simple for loop
            for (int i = wordAsArray.Length - 1; i >= 0; i--)
            {
                reversed += wordAsArray[i];
            }
            return word == reversed;
        }
    }
}
