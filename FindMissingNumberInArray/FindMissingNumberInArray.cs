﻿using System;

namespace FindMissingNumberInArray
{
    /**
     * You are given a list of n-1 integers and these integers are in the range of 1 to n. 
     * There are no duplicates in list. One of the integers is missing in the list. 
     * Write an efficient code to find the missing integer.
     * */
    class FindMissingNumberInArray
    {
        static void Main(string[] args)
        {
            int[] sortedArray = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 10 };
            int missingNumber = MissingNumber(sortedArray, 10);
            Console.WriteLine(missingNumber);
            Console.ReadKey();
        }

        static int MissingNumber(int[] array, int n) {

            int expectedSum = 0;

            // ∑ =(n^2 + n) / 2
            // https://en.wikipedia.org/wiki/Triangular_number

            expectedSum = n * (n + 1) / 2;

            for (int i = 0; i < array.Length; i++) {
                expectedSum -= array[i];
            }

            return expectedSum;
        }

    }
}
