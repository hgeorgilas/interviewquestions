﻿using System;
using System.Collections.Generic;

namespace CountTreeLeaves
{
    class Program
    {
        static void Main(string[] args)
        {
            Node Aleft = new Node(1, null, null);
            Node Aright = new Node(6, null, null);
            Node A = new Node(4, Aleft, Aright);

            Node Bleft = new Node(7, null, null);
            Node Bright = new Node(12, null, null);
            Node B = new Node(9, Bleft, Bright);

            Node root = new Node(7, A, B);

            var resultRec = CountLeavesRecursively(root);
            var resultIter = CountLeavesIteratively(root);
            Console.WriteLine(resultRec);
            Console.WriteLine(resultIter);
            Console.ReadLine();
        }

        public static int CountLeavesRecursively(Node node)
        {
            if (node == null) return 0;
            if (node.IsLeaf())
            {
                return 1;
            }
            else
            {
                return CountLeavesRecursively(node.left) + CountLeavesRecursively(node.right);
            }
        }


        public static int CountLeavesIteratively(Node node)
        {
            if (node == null)
            {
                return 0;
            }

            int count = 0;

            Stack<Node> stack = new Stack<Node>();
            stack.Push(node);

            while (stack.Count != 0)
            {
                Node popped = stack.Pop();
                if (popped.left != null)
                {
                    stack.Push(popped.left);
                }
                if (popped.right != null)
                {
                    stack.Push(popped.right);
                }
                if (popped.IsLeaf())
                {
                    count++;
                }
            }

            return count;
        }
    }

    class Node
    {
        public int data { get; set; }
        public Node left { get; set; }
        public Node right { get; set; }

        public Node(int data, Node left, Node right)
        {
            this.data = data;
            this.left = left;
            this.right = right;
        }

        public bool IsLeaf()
        {
            return this.left == null && this.right == null;
        }

    }
}
