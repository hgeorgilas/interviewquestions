﻿using System;

namespace FindMinMaxInUnsortedArray
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] array = new int[] { 1, 2, 3, 4, 5, 67, 8, 34, 5 };
            FindMinMax(array);
            Console.ReadLine();
        }


        static void FindMinMax(int[] array)
        {
            var min = Int32.MaxValue;
            var max = Int32.MinValue;

            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] > max)
                {
                    max = array[i];
                }
                if (array[i] < min)
                {
                    min = array[i];
                }
            }

            Console.WriteLine("Max: " + max);
            Console.WriteLine("Min: " + min);
        }
    }
}
