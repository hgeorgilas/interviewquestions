﻿using System;
using System.Collections.Generic;

namespace MostFrequentNumberInArray
{
    class MostFrequentNumberInArray
    {
        static void Main(string[] args)
        {
            // we should find that max number is 8.
            int[] array = new int[] { 1, 2, 3, 5, 6, 7, 8, 8, 8, 8, 9, 9, 2, 3, 4, 5 }; 
            FindMostFrequentNumber(array);
            Console.ReadLine();
        }

        static void FindMostFrequentNumber(int[] array)
        {
            Dictionary<int, int> dictionary = new Dictionary<int, int>();

            for (int i = 0; i < array.Length; i++)
            {
                if (dictionary.ContainsKey(array[i]))
                {
                    dictionary[array[i]]++;
                }
                else
                {
                    dictionary[array[i]] = 1;
                }
            }

            int index = 0;
            int maxNumber = 0;
            foreach (KeyValuePair<int, int> pair in dictionary)
            {
                if (maxNumber < pair.Value)
                {
                    maxNumber = pair.Value;
                    index = pair.Key;
                }
            }

            Console.WriteLine(index+" found "+maxNumber+" times");
        }
    }
}
