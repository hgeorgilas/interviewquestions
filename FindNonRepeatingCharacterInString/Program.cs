﻿using System;
using System.Collections.Generic;

namespace FindNonRepeatingCharacterInString
{
    //find FIRST non repeating char in string
    class FindNonRepeatingCharacterInString
    {
        static void Main(string[] args)
        {
            string word = "aaaabbbbcddffeffferet";
            string result = NonRepeatingChar(word);
            Console.WriteLine(result);
            Console.ReadLine();
        }

        static string NonRepeatingChar(string word)
        {
            char[] array = word.ToCharArray();

            string result = "Nothing found";

            Dictionary<char, int> dic = new Dictionary<char, int>();

            for (int i = 0; i < array.Length; i++)
            {
                if (dic.ContainsKey(array[i]))
                {
                    dic[array[i]]++;
                }
                else
                {
                    dic[array[i]] = 1;
                }
            }

            foreach (KeyValuePair<char, int> entry in dic)
            {
                if (entry.Value == 1)
                {
                   result = entry.Key.ToString();
                   break;
                }
            }

            return result;
        
        }
    }
}
