﻿using System;

namespace ReverseArray
{
    class ReverseArray
    {
        static void Main(string[] args)
        {
            int size = 10;
            int[] array = GenerateArray(size);

            Console.WriteLine("Starting array");
            PrintArray(array);

            Console.WriteLine(new String('-', 20));

            Console.WriteLine("Using new array");
            ReverseArrayUsingNewArray(array);

            Console.WriteLine("In Place array");
            ReverseArrayInPlace(array);

            Console.ReadLine();
        }

        // Time complexity O(n), Space Complexity O(n)
        static void ReverseArrayUsingNewArray(int[] array)
        {
            int[] newArray = new int[array.Length];

            for (int i = array.Length - 1; i >= 0; i--)
            {
                newArray[(array.Length - 1) - i] = array[i];
            }
            PrintArray(newArray);
        }

        //Time complexity O(logn), Space Complexity O(1);
        static void ReverseArrayInPlace(int[] array)
        {

            for (int i = (array.Length - 1) / 2; i >= 0; i--)
            {
                var temp = array[(array.Length - 1) - i];
                array[(array.Length - 1) - i] = array[i];
                array[i] = temp;
                //newArray[(array.Length - 1) - i] = array[i];
            }
            PrintArray(array);
        }


        static void PrintArray(int[] array)
        {
            Console.WriteLine(string.Join(',', array));
        }

        static int[] GenerateArray(int size)
        {

            int[] array = new int[size];
            Random rnd = new Random();

            for (int i = 0; i < array.Length; i++)
            {
                array[i] = rnd.Next(1, size);
            }

            return array;
        }
    }
}
