﻿using System;

namespace StringIsOneChangeAway
{
    // check if 2 words are one change away.
    // By 'one change' away meaning
    // inserting or deleting a letter 
    // to one or the other word
    class Program
    {
        static void Main(string[] args)
        {
            string stringA = "abcdef";
            string stringB = "abceaf";

            Console.WriteLine(IsOneChangeAway(stringA, stringB));

            Console.ReadLine();

        }

        static bool IsOneChangeAway(string wordA, string wordB)
        {
            //if length diff > 2 return false.
            var lengthDiff = wordA.Length - wordB.Length;
            if (lengthDiff > 2) return false;


            int changesRequired = 0;

            if (lengthDiff == 0)
            {
                return IsOneAwaySameLength(wordA, wordB);
            }
            else if (lengthDiff == 1)
            {
                return IsOneAwayDifferentLength(wordA, wordB);
            }

            return false;

        }

        static bool IsOneAwaySameLength(string wordA, string wordB)
        {
            char[] wordAArray = wordA.ToCharArray();
            char[] wordBArray = wordB.ToCharArray();
            int changesRequired = 0;
            for (int i = 0; i < wordAArray.Length; i++)
            {
                if (wordAArray[i] != wordBArray[i])
                {
                    changesRequired++;
                    if (changesRequired > 1)
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        static bool IsOneAwayDifferentLength(string wordA, string wordB)
        {
            char[] wordAArray = wordA.ToCharArray();
            char[] wordBArray = wordB.ToCharArray();
            char[] longestLengthArray = wordAArray.Length > wordBArray.Length ? wordAArray : wordBArray;
            char[] smallestLengthArray = wordAArray.Length < wordBArray.Length ? wordAArray : wordBArray;

            int indexCounter = 0;
            int changesRequired = 0;
            while (smallestLengthArray.Length > indexCounter)
            {
                if (longestLengthArray[indexCounter + changesRequired] == smallestLengthArray[indexCounter])
                {
                    indexCounter++;
                }
                else
                {
                    changesRequired++;
                    if (changesRequired > 1) { return false; }
                }
            }

            return true;
        }
    }
}
